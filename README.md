# GitlabCertbotDigitalOcean

The intent of this image is to allow for hosted gitlab pages to have https with custom domains through certbot SSL and Digital Ocean DNS.

This is based on the work of [Michael Ummels](https://www.ummels.de/2018/09/03/gitlab-lets-encrypt/)



## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

## Built With

* [Docker](https://www.docker.com) - Containers


### Prerequisites

You should be hosting your website on [gitlab](https://gitlab.com) and use a .gitlab-ci.yaml file to deploy.

```yaml
variables:
  LC_ALL: "C.UTF-8"
  DOMAIN: "yourdomain.com"

stages:
  - deploy
  - update

pages:
  image: alpine:latest
  stage: deploy
  script:
    - mkdir public
    - cp -a deployment/client/. public/
  artifacts:
    paths:
      - public
  only:
    - master
  except:
    - schedules

update_cert:
  image:
    name: thecb4/gitlabcertbotdigitalocean
    entrypoint: [""]
  variables:
    CERT_FILE: "/etc/letsencrypt/live/$DOMAIN/fullchain.pem"
    KEY_FILE: "/etc/letsencrypt/live/$DOMAIN/privkey.pem"
  stage: update
  before_script:
    - echo "Adding digital ocean token"
    - echo "dns_digitalocean_token = $DIGITAL_OCEAN_TOKEN" > .secret
    - chmod 600 .secret
  script:
    - bin/certbot.sh "$DOMAIN"
    - |
        curl --silent --fail --show-error --request PUT --header \"Private-Token: $GITLAB_CI_TOKEN\" --form \"certificate=@$CERT_FILE\" --form \"key=@$KEY_FILE\" "https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/pages/domains/$DOMAIN"
  only:
    - schedules
```

The certbot.sh

```sh
#!/usr/bin/env sh

certbot certonly \
--non-interactive \
--agree-tos \
--server "https://acme-staging-v02.api.letsencrypt.org/directory" \
--dns-digitalocean --dns-digitalocean-credentials .secret \
--dns-digitalocean-propagation-seconds 60 \
--email "you@youremail.com" \
-d "*.$1"
```

And your .secret file should look something like

```
# DigitalOcean API credentials used by Certbot
dns_digitalocean_token = 0000111122223333444455556666777788889999aaaabbbbccccddddeeeeffff
```

### Installing

Push your code to the server

```
git push origin master
```

## Using

Set up a schedule on gitlab to run on the 1st of every month.

### Break down into end to end tests

Explain what these tests test and why

```
Give an example
```

## Roadmap and Contributing

### Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags).



### Roadmap

Please read [ROADMAP](ROADMAP.md) for an outline of how we would like to evolve the library.

### Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code of conduct, and the process for changes to us.

### Changes

Please read [CHANGELOG](CHANGELOG.md) for details on changes to the library.


## Authors

* **'Michael Ummels'** - *Original work* - [ummels](https://www.ummels.de)
* **'Cavelle Benjamin'** - *Adapted for Digital Ocean DNS Challenge* - [thecb4](https://thecb4.io)

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc
